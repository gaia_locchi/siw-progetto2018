package it.uniroma3.repository;


import org.springframework.data.repository.CrudRepository;

import it.uniroma3.model.Allievo;

public interface AllievoRepository extends CrudRepository<Allievo, Long> {

	Allievo findByNomeAndCognome(String nome, String cognome);

}
