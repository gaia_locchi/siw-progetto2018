package it.uniroma3.controller.validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.model.Allievo;

@Component
public class AllievoValidator implements Validator {

	@Override
	public boolean supports(Class<?> aClass) {
		
		return Allievo.class.equals(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cognome", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dataDiNascita", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "luogoDiNascita", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "telefono", "required");
		
		if(!errors.hasFieldErrors("telefono")) {
			
			try {
				
				Integer.parseInt((String)errors.getFieldValue("telefono"));
			}
			catch(NumberFormatException e) {
				
				errors.rejectValue("telefono", "numeric");
			}
		}
		
		if(!errors.hasFieldErrors("dataDiNascita")) {
			try {
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
				@SuppressWarnings("unused")
				Date parsed = format.parse((String) errors.getFieldValue("dataDiNascita"));
			}
			catch(ParseException e) {
				
				errors.rejectValue("dataDiNascita", "format");
			}
		}
	}

}
