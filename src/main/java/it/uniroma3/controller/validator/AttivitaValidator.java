package it.uniroma3.controller.validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.model.Attivita;

@Component
public class AttivitaValidator implements Validator {

	@Override
	public boolean supports(Class<?> aClass) {

		return Attivita.class.equals(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dataOra", "required");
		
		if(!errors.hasFieldErrors("dataOra")) {
			try {
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm");
				@SuppressWarnings("unused")
				Date parsed = format.parse((String) errors.getFieldValue("dataOra"));
			}
			catch(ParseException e) {
				
				errors.rejectValue("dataOra", "format");
			}
		}
	}

}
