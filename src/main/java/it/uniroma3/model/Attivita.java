package it.uniroma3.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Attivita {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable=false)
	private String nome;

	@Column(nullable=false)
	private String dataOra;

	@ManyToMany
	private List<Allievo> allievi;

	@ManyToOne
	private Centro centro;
	
	public Attivita() {
		this.allievi = new ArrayList<Allievo>();
	}
	
	public Attivita(String nome, String dataOra) {
		this.nome = nome;
		this.dataOra = dataOra;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDataOra() {
		return dataOra;
	}

	public void setDataOra(String dataOra) {
		this.dataOra = dataOra;
	}
	
	public void addAllievo(Allievo allievo) {
		this.allievi.add(allievo);
	}

}
